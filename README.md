# Mycv

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.3

It was bootstrapped with [startbootstrap](https://startbootstrap.com/template-overviews/resume/) then it was enriched with [firebase](https://firebase.google.com/) and [lichess.org](https://lichess.org/).
Some JSON data stored at firebase and some are local. Integration with lichess.org is my hobby and if you like chess you may like it though.

There are two folders under the assets folder, '**data**' and '**img**'. 
The data folder contains some JSON files which should be tailored upon your 
preferences for the sections of about, skills, education and certifications. 
If your certification doesn't have a url then you can download it under data 
folder and save the url into the JSON as 'assets/data/cert_google_cloud_essential.pdf'.
If it is photo then better to save it to the img folder as 'assets/img/cert_stc.png'. 
The img folder contains some images which should be customized, too.

[Firebase](https://firebase.google.com/) part can be implemented with mycv.json sample data file.
_create your real-time database in firebase and import mycv.json into it; thereafter, set the rules for the security. That's all. Note that if you will not use the [lichess.org](https://lichess.org/) integration, you should drop the chess-players section inside the JSON file.

[lichess.org](https://lichess.org/) integration is referenced on the [https://lichess.org/api](https://lichess.org/api). The data fetched from **lichess** in real-time and broadcasted to the pages.

The last but not least, [Google Tag Manager](https://marketingplatform.google.com/about/tag-manager/) is used to monitor the audience.
You should replace my google tag manager related scripts in the index.html with yours. Especially, the id part 'GTM-XXXXXXX'

If you have further inquiries, feel free to send me an email with a clear prefix in the subject such as 'my cv: ...'
ozan.senturk at gmail.com

In return I request you to keep my footer tag at the bottom of your page as an [MIT License](https://choosealicense.com/licenses/mit) 



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## License
[MIT](https://choosealicense.com/licenses/mit/)
