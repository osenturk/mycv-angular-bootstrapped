import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoggingService} from '../logging.service';
import {DataService} from '../shared/data.service';
import {LichessRequestModel} from './model/lichess.request.model';
import {LichessService} from './lichess.service';
import {LichessPlayeridModel} from './model/lichess.playerid.model';
import {LichessActivityModel} from './model/lichess.activity.model';
import {LichessGameModel} from './model/lichess.game.model';
import {Subject, Subscription} from 'rxjs';
import {LichessUserModel} from './model/lichess.user.model';

@Component({
  selector: 'app-chess',
  templateUrl: './chess.component.html',
  styleUrls: ['./chess.component.css'],
  providers: [ DataService, LichessService]
})
export class ChessComponent implements OnInit, OnDestroy {

  private subscriptionGetFollowers: Subscription;
  private subscriptionGetActivity: Subscription;
  private subscriptionGetGames: Subscription;
  private subscriptionGetUsers: Subscription;

  /*
  * subject is the most important operator to push the data retrieved from the server
  * */
  private subjectFollowers = new Subject<LichessPlayeridModel[]> ();

  lichessRequest: LichessRequestModel = new LichessRequestModel(
    'sentrworks', true, 20, false, true, false, false, true
  );

  choosenUser: string;
  choosenGame: string;

  selectedGames: LichessGameModel [];
  myFollowers: LichessPlayeridModel[] = [];

  filteredPlayer = '';

  activities: LichessActivityModel[] = [];

  users: LichessUserModel[] = [];

  selectedUser(username: string) {
    this.choosenUser = username;
    this.lichessRequest.username = username;

    this.subscriptionGetGames = this.lichessService.getGames(this.lichessRequest)
      .subscribe(
        (response) => {

          console.log('Get Games for ' + username );
          this.selectedGames = response;
        }
        // (err) => console.log(err)
      );
  }



  openModal(gameId: string) {

    this.choosenGame = 'https://lichess.org/embed/' + gameId + '?theme=auto&bg=auto';

  }

  sortyBy(column: string) {
    this.users.sort((a, b) => {

      if ( b.seenAt > a.seenAt) {
        return 1;
      }
      if ( b.seenAt < a.seenAt) {
        return -1;
      }

      return 0;
    });

  }

  constructor( private loggingService: LoggingService, private dataService: DataService, private lichessService: LichessService) {
    this.choosenUser = 'sentrworks';
    this.selectedGames = [];

  }

  ngOnInit(): void {



    this.subscriptionGetFollowers = this.dataService.getChessPlayers()
      .subscribe(
        (followers: LichessPlayeridModel[]) => {

          console.log('the size of the followers ' + followers.length );

          // this.subjectFollowers.next(followers);

          let letMeString: string[] = [];

          for (let follower of followers) {
            letMeString.push(follower.id);
          }

          this.subscriptionGetUsers = this.lichessService.getUsers( letMeString.toString()).subscribe(
            response => {
              //console.log(response);
              this.users = response;
            }

          );
        }
        // (err) => console.log(err)
      );



  }


ngOnDestroy(): void {

    this.subscriptionGetGames.unsubscribe();
  this.subscriptionGetActivity.unsubscribe();
  this.subscriptionGetFollowers.unsubscribe();
  this.subscriptionGetUsers.unsubscribe();
  console.log('unsubscribed from all the services ..');
}


}
