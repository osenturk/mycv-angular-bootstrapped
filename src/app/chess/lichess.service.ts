import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {Observable, pipe, Subject, throwError} from 'rxjs';

import {LichessRequestModel} from './model/lichess.request.model';
import {LichessGameModel} from './model/lichess.game.model';
import {LichessActivityModel} from './model/lichess.activity.model';
import {LichessPlayeridModel} from './model/lichess.playerid.model';
import {LichessIntervalModel} from './model/lichess.interval.model';
import {LichessSummaryModel} from './model/lichess.summary.model';
import {LichessRatingModel} from './model/lichess.rating.model';
import {LichessActivitytypeModel} from './model/lichess.activitytype.model';
import {LichessUserModel} from './model/lichess.user.model';
import {LichessOpeningModel} from './model/lichess.opening.model';

@Injectable()
export class LichessService {

  requestHost: string;

  subjectFollowers = new Subject<LichessPlayeridModel>();

  constructor(private httpClient: HttpClient) {
      this.requestHost = 'https://lichess.org';
  }

  getGames(lichessRequest: LichessRequestModel): Observable <LichessGameModel[]> {

    const params =  new HttpParams()
      .append('rated', lichessRequest.rated ? 'true' : 'false')
      .append('max', lichessRequest.max + '' )
      .append('ongoing', lichessRequest.ongoing ? 'true' : 'false')
      .append('moves', lichessRequest.moves ? 'true' : 'false')
      .append('tags',   lichessRequest.tags ? 'true' : 'false')
      .append('evals',  lichessRequest.evals ? 'true' : 'false')
      .append('opening', lichessRequest.opening ? 'true' : 'false')
      .append('username', lichessRequest.username)



    // const authorizePath = 'api/account';
    const requestApi = this.requestHost + '/api/games/user/' + lichessRequest.username;

    return this.httpClient.get(requestApi, {
      params:  params,
      headers: new HttpHeaders({'accept': 'application/x-ndjson'}),
      responseType: 'text'
  })
      .pipe(

        map((response: string) => {

          // console.log(response);

          const gamesStr: string[] = response.split('\n');

          const games: LichessGameModel[] = [];

          for (let game of gamesStr) {
            if (game.length > 0) {
              const oneGame: LichessGameModel = JSON.parse(game);
              oneGame.createdAt = new Date(oneGame.createdAt);
              oneGame.lastMoveAt = new Date(oneGame.lastMoveAt);

              if (['draw', 'resign', 'mate', 'started', 'outoftime', 'timeout', 'stalemate'].indexOf(oneGame.status) < 0 ) {
                oneGame.opening = new LichessOpeningModel('na', 'na', 0);
              }
              if (['draw', 'outoftime', 'stalemate'].indexOf(oneGame.status) > -1 ) {
                if (oneGame.winner === undefined ) {
                  oneGame.winner = 'draw';
                }
              }

              if (oneGame.status === 'started' )  {

                oneGame.winner = 'ongoing';

              }

              games.push(oneGame);
            }
          }

          console.log('game 2nd player name: ' + games[0].players['black'].user.name);



        return games;

      })
        // ,
        // catchError(this.handleError)

      );

  }

  getActivity(username: string): Observable<LichessActivityModel> {

    const params =  new HttpParams()
      .append('username', username);

    const requestApi = this.requestHost + '/api/user/' + username + '/activity';

    return this.httpClient.get<LichessActivityModel>(requestApi, {
      headers: new HttpHeaders({'accept': 'application/json'})

    })

      .pipe(

        map((response) => {

          // console.log('interval relative' + response[0].interval.start );

          const games = response[0]['games'];
          const keys = Object.keys(games);
          const values = Object.values(games);

          const rating = new LichessRatingModel(values[0]['rp']['before'], values[0]['rp']['after'] );
          const summary: LichessSummaryModel = new LichessSummaryModel(values[0]['win'], values[0]['loss'], values[0]['draw']);

          const activityType = new LichessActivitytypeModel(keys[0], summary, rating);

          const activity: LichessActivityModel = new LichessActivityModel(
            username,
            new LichessIntervalModel(
              new Date(response[0].interval.start * 1000), new Date(response[0].interval.end * 1000) ),
            activityType

          );

          console.log('interval start ' + activity.interval.start );
          console.log('activity name ' + activity.activityType.activityName );



          return activity;

        })
        // ,
        // catchError(this.handleError)

      );

  }





  getFollowers(username: string): Observable<LichessPlayeridModel[]> {

    const params =  new HttpParams()
      .append('username', username);

    const requestApi = this.requestHost +  '/api/user/' + username + '/following';

    return this.httpClient.get(requestApi, {
      headers: new HttpHeaders({'accept': 'application/x-ndjson'}),
      responseType: 'text'

    })

      .pipe(

        map((response: string) => {

          // console.log(response);

          const followerStr: string[] = response.split('\n');

          const followers: LichessPlayeridModel[] = [];

          for (let follower of followerStr) {
            if (follower.length > 0) {

              const tempFollower = JSON.parse(follower);
              const oneFollower = new LichessPlayeridModel(tempFollower['id'], tempFollower['username']);

              followers.push(oneFollower);
            }
          }

          console.log('game 2nd follower name: ' + followers[1].name);
          console.log('game 3rd follower id: ' + followers[2].id);


          return followers;

        })
        // ,
        // catchError(this.handleError)

      );

  }

  getUsers(lichessPlayers: string): Observable <LichessUserModel[]> {

    // const authorizePath = 'api/account';
    const requestApi = this.requestHost + '/api/users';

    return this.httpClient.post<LichessUserModel[]>(requestApi,
      lichessPlayers
    ).pipe(
      map(users => {


          const myUsers: LichessUserModel[] = [];

          for (let user of users) {

            console.log('current user: ' + user['username']);

              const oneUser = new LichessUserModel(user['id'], user['username'], user['online'],
                 user['playTime']['total'], user['streaming'], user['seenAt'], user['patron'],

                (user['perfs']['blitz'] !== undefined) ? user['perfs']['blitz']['rating'] : 1500,
                (user['perfs']['bullet'] !== undefined) ? user['perfs']['bullet']['rating'] : 1500,
                (user['perfs']['rapid'] !== undefined) ? user['perfs']['rapid']['rating'] : 1500,
                (user['perfs']['puzzle'] !== undefined) ? user['perfs']['puzzle']['rating'] : 1500,
                (user['perfs']['classical'] !== undefined) ? user['perfs']['classical']['rating'] : 1500,
                );

              myUsers.push(oneUser);

          }

          // descending by blitz

          myUsers.sort((a, b) => {

            if ( b.blitz > a.blitz) {
              return 1;
            }
            if ( b.blitz < a.blitz) {
              return -1;
            }

            return 0;
          });

        return myUsers;


      }


      )

    );



  }


  private handleError(err: HttpErrorResponse) {
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', err.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${err.status}, ` +
        `body was: ${err.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


}
