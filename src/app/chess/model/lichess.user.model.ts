export class LichessUserModel {


  id: string;
  username: string;
  online: boolean;
  playTime: number;
  streaming: boolean;
  seenAt: Date;
  blitz?: number;
  bullet?: number;
  rapid?: number;
  puzzle?: number;
  patron: boolean;
  classical?: number;

  constructor(id: string, username: string, online: boolean, playTime: number, streaming: boolean, seenAt: Date, patron: boolean,
              blitz?: number, bullet?: number, rapid?: number, puzzle?: number, classical?: number) {

    this.id = id;
    this.online = online;
    this.playTime = playTime;
    this.streaming = streaming;
    this.username = username;
    this.seenAt = seenAt;
    this.blitz = blitz;
    this.bullet = bullet;
    this.rapid = rapid;
    this.puzzle = puzzle;
    this.patron = patron;
    this.classical = classical;
  }

}
