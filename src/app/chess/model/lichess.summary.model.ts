export class LichessSummaryModel {

  win: number;
  draw: number;
  loss: number;

  constructor (win: number, draw: number, loss: number) {
    this.win = win;
    this.loss = loss;
    this.draw = draw;
  }

}
