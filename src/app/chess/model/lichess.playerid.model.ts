export class LichessPlayeridModel {

  id: string;
  name: string;
  patron?: boolean;

  constructor (id: string, name: string, patron?: boolean) {
    this.id = id;
    this.name = name;
    this.patron = patron;

  }

}
