import {ActivityModel} from './activity.model';
import {GameModel} from './game.model';

export class ChessModel {

  public username: string;
  public activity: ActivityModel;
  public updated: Date;
  public games: GameModel[];

  constructor (username: string, activity: ActivityModel, updated: Date, games: GameModel[]) {
    this.username = username;
    this.activity = activity;
    this.updated = updated;
    this.games = games;
  }

  public setGames(games: GameModel[]) {
    this.games = games;
  }


}
