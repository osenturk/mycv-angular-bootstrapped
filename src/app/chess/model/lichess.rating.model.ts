export class LichessRatingModel {

  before: number;
  after: number;

  constructor (before: number, after: number) {
    this.before = before;
    this.after = after;
  }
}
