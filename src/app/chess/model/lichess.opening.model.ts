export class LichessOpeningModel {

  eco: string;
  name: string;
  ply: number;

  constructor (eco: string, name: string, ply: number) {
    this.eco = eco;
    this.name = name;
    this.ply = ply;
  }
}
