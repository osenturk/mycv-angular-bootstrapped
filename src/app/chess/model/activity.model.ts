export class ActivityModel {

  public online: string;
  public offline: string;
  public type: string;
  public win: string;
  public loss: string;
  public draw: string;
  public before: string;
  public after: string;

  constructor (online: string, offline: string, type: string, win: string, loss: string, draw: string, before: string, after: string) {
    this.online = online;
    this.offline = offline;
    this.type = type;
    this.win = win;
    this.loss = loss;
    this.draw = draw;
    this.before = before;
    this.after = after;
  }
}
