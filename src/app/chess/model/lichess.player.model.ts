import {LichessPlayeridModel} from './lichess.playerid.model';

export class LichessPlayerModel {

  colour: string;
  user: LichessPlayeridModel;
  rating: number;
  ratingDiff: number;

  constructor(colour: string, user: LichessPlayeridModel, rating: number, ratingDiff: number) {
    this.colour = colour;
    this.user = user;
    this.rating = rating;
    this.ratingDiff = ratingDiff;

  }


}
