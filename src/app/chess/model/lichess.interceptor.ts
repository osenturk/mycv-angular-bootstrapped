import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

export class LichessInterceptor implements HttpInterceptor {

  requestHost = 'https://lichess.org';
  token = 'FCD9VvBxqO8jKvhG';


  intercept( req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (req.url.indexOf('lichess') > -1) {

      console.log('this is a lichesss request ', req);
      const copiedReq = req.clone(
      {
        headers: req.headers.append('Authorization', 'Bearer ' + this.token)
      }
      );

      return next.handle(copiedReq);

    } else {

      console.log('not a lichess request: ', req.url);
      return next.handle(req);

    }

  }



}
