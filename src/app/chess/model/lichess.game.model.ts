import {LichessPlayerModel} from './lichess.player.model';
import {LichessOpeningModel} from './lichess.opening.model';
import {LichessClockModel} from './lichess.clock.model';

export class LichessGameModel {


   id: string;
   rated: boolean;
   variant: string;
   speed: string;
   perf: string;

   createdAt: Date;
   lastMoveAt: Date;
   status: string;

   players: LichessPlayerModel[];
   winner?: string;
   opening?: LichessOpeningModel;
   clock: LichessClockModel;

   constructor(id: string, rated: boolean, variant: string, speed: string,
               perf: string, createdAt: Date, lastMoveAt: Date, status: string,
               players: LichessPlayerModel[],  clock: LichessClockModel, opening?: LichessOpeningModel, winner?: string) {

     this.id = id;
     this.opening = opening;
     this.clock = clock;
     this.createdAt = createdAt;
     this.lastMoveAt = lastMoveAt;
     this.perf = perf;
     this.players = players;
     this.rated = rated;
     this.speed = speed;
     this.variant = variant;
     this.status = status;
     this.winner = winner;

  }

}
