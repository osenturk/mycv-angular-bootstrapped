import {LichessSummaryModel} from './lichess.summary.model';
import {LichessRatingModel} from './lichess.rating.model';

export class LichessActivitytypeModel {

  activityName: string;

  summary: LichessSummaryModel;
  ratingPoint: LichessRatingModel;

  constructor( activityName: string, summary: LichessSummaryModel, ratingPoint: LichessRatingModel) {

    this.activityName = activityName;
    this.summary = summary;
    this.ratingPoint = ratingPoint;

  }
}
