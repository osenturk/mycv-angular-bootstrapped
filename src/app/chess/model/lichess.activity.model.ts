import {LichessIntervalModel} from './lichess.interval.model';
import {LichessActivitytypeModel} from './lichess.activitytype.model';

export class LichessActivityModel {

  username: string;
  interval: LichessIntervalModel;
  activityType: LichessActivitytypeModel;
  patron ?: boolean;

  constructor(username: string, interval: LichessIntervalModel, activityType: LichessActivitytypeModel, patron?: boolean) {

    this.username = username;
    this.activityType = activityType;
    this.interval = interval;
    this.patron = patron;

  }

}
