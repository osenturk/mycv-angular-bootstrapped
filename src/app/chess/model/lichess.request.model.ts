export class LichessRequestModel {

   username: string;
   rated: boolean;
   max: number;
   moves: boolean;
   ongoing: boolean;
   tags: boolean;
   evals: boolean;
   opening: boolean;

  constructor(username: string, rated: boolean,
              max: number, moves: boolean, ongoing: boolean,
              tags: boolean, evals: boolean, opening: boolean) {

    this.username = username;
    this.rated = rated;

    this.max = max > 20 ? 20 : max;

    this.moves = moves;
    this.ongoing = ongoing;
    this.tags = tags;
    this.evals = evals;
    this.opening = opening;

  }

}
