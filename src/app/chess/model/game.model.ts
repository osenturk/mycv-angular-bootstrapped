export class GameModel {

  public time: Date;
  public type: string;
  public game_id: string;
  public white_id: string;
  public white_rating: string;
  public black_id: string;
  public black_rating: string;
  public status: string;
  public time_initial: number;
  public time_increment: number;

  constructor(time: Date, type: string, game_id: string, white_id: string, white_rating: string,
              black_id: string, black_rating: string, status: string, time_initial: number,
              time_increment: number) {

    this.time = time;
    this.type = type;
    this.game_id = game_id;
    this.white_id = white_id;
    this.white_rating = white_rating;
    this.black_id = black_id;
    this.black_rating = black_rating;
    this.status = status;
    this.time_initial = time_initial;
    this.time_increment = time_increment;
  }
}
