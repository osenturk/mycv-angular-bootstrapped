import { Pipe, PipeTransform } from '@angular/core';
import {el} from '@angular/platform-browser/testing/src/browser_util';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string ): any {

    if (value.length === 0 || filterString === '') {
      return value;
    }

    const resultArray = [];

    /* partial search was added to catch any skills */

    /* a string propName switch was added to use the same filter in many points. If there is no
     * propName provided then it will be used just for skills */

    for (const item of value) {

      if (propName.length === 0) {
        for (const askill of item.skillSet) {

          if (askill.toUpperCase().indexOf(filterString.toUpperCase()) > -1) {
            if (resultArray.includes(item) === false) {
              resultArray.push(item);
            }

          }

        }
      } else {
        if (item[propName].toUpperCase().indexOf(filterString.toUpperCase()) > -1) {
          if (resultArray.includes(item) === false) {
            resultArray.push(item);
          }
        }

      }

    }

    return resultArray;

  }

}
