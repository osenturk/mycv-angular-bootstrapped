import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {LoggingService} from '../logging.service';
import {SkillModel} from '../skills/skill.model';

import { map } from 'rxjs/operators';
import {Observable, pipe, throwError} from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {PodcastModel} from '../podcasts/podcast.model';
import {LichessPlayeridModel} from '../chess/model/lichess.playerid.model';
import {AwardsModel} from '../awards/awards.model';
import {AboutModel} from '../about/about.model';
import {EducationModel} from '../education/education.model';


@Injectable()
export class DataService {

  constructor (private httpClient: HttpClient, private loggingService: LoggingService) {}


  getSkills(): Observable<SkillModel[]> {
    return this.httpClient.get<SkillModel[]>('assets/data/skills.json', {
     observe: 'body',
     responseType: 'json'
    })
      .pipe(
        map(
        // catchError(this.handleError)
        (skills: SkillModel[]) => {
          for (let skill of skills) {
            if (!skill['skillSet']) {
              skill['skillSet'] = [];
            }
          }
          return skills['skills'];
        })

      );
  }

  /*
  firebase doesn't need indexing such as podcasts["podcasts"] but local JSONs need such as awards["awards"]
   */
  getPodcasts(): Observable<PodcastModel[]> {
    return this.httpClient.get<PodcastModel[]>('https://mycv-02122018.firebaseio.com/podcasts.json', {
      observe: 'body',
      responseType: 'json'
    })
      .pipe(
        map(
          // catchError(this.handleError)
          (podcasts: PodcastModel[]) => {

            // console.log('podcasts 0 ' + podcasts)

            podcasts.sort((a, b) => {



              if ( b.date > a.date) {
                return 1;
              }
              if ( b.date < a.date) {
                return -1;
              }

              return 0;
            });

            return podcasts;
          })

      );
  }


  getChessPlayers(): Observable<LichessPlayeridModel[]> {
    return this.httpClient.get<LichessPlayeridModel[]>('https://mycv-02122018.firebaseio.com/chess-players.json', {
      observe: 'body',
      responseType: 'json'
    });

  }

  getAwards(): Observable<AwardsModel[]> {
    return this.httpClient.get<AwardsModel[]>('assets/data/awards.json', {
      observe: 'body',
      responseType: 'json'
    })
      .pipe(
        map(
          // catchError(this.handleError)
          (awards: AwardsModel[]) => {

            awards['awards'].sort((a, b) => {

              if ( b.date > a.date) {
                return 1;
              }
              if ( b.date < a.date) {
                return -1;
              }

              return 0;
            });

            return awards['awards'];
          })

      );
  }

  getAbout(): Observable<AboutModel> {
    return this.httpClient.get<AboutModel>('assets/data/about.json', {
      observe: 'body',
      responseType: 'json'
    })
      .pipe(
        map(
          // catchError(this.handleError)
          (about: AboutModel) => {

            return about['about'];

          })
      );
  }
  getEducation(): Observable<EducationModel[]> {
    return this.httpClient.get<EducationModel[]>('assets/data/about.json', {
      observe: 'body',
      responseType: 'json'
    })
      .pipe(
        map(
          // catchError(this.handleError)
          (educations: EducationModel[]) => {

            return educations['education'];

          })
      );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
    }
}
