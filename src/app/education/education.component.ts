import {Component, OnInit} from '@angular/core';
import {LoggingService} from '../logging.service';
import {DataService} from '../shared/data.service';
import {EducationModel} from './education.model';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css'],
  providers: [DataService]
})
export class EducationComponent implements OnInit {

  constructor(  private loggingService: LoggingService, private dataService: DataService) {}

   educations: EducationModel[] = [

    new EducationModel(
      'WBS',
      'https://www.wbs.ac.uk',
      'Coventry',
      'West Midlands',
      'United Kingdom',
      'MSc. Business Analytics',
       new Date('2017-10-02'),
      new Date('2018-10-01'),
      'GPA',
      'Merit') ];


  ngOnInit() {

    this.loggingService.logToConsole('education is initialized');

    this.dataService.getEducation()

      .subscribe(
        educations => {

          this.educations = educations;

        }, (error: string) => {
          this.loggingService.logToConsole('error: ' + error); }

      );

  }

}
