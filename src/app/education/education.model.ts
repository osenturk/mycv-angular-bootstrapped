
export class EducationModel {

  public name: string;
  public url: string;
  public city: string;
  public county: string;
  public country: string;
  public programme: string;
  public startDate: Date;
  public finishDate: Date;
  public gpaLabel: string;
  public gpaScore: string;


  constructor (name: string, url: string, city: string, county: string, country: string, programme: string,
                startDate: Date, finishDate: Date, gpaLabel: string, gpaScore: string ) {
    this.name = name;
    this.url = url;
    this.city = city;
    this.country = country;
    this.county = county;
    this.programme = programme;
    this.startDate = startDate;
    this.finishDate = finishDate;
    this.gpaLabel = gpaLabel;
    this.gpaScore = gpaScore;

  }

}
