export class PodcastModel {

  public name: string;
  public url: string;
  public author: string;
  public date: Date;
  public category: string;
  public podcastId: string;

  constructor (name: string, url: string, author: string, date: Date, category: string, podcastId: string) {
    this.name = name;
    this.url = url;
    this.author = author;
    this.date = date;
    this.category = category;
    this.podcastId = podcastId;
  }

}
