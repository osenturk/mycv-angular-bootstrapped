import { Component, OnInit } from '@angular/core';
import {LoggingService} from '../logging.service';
import {DataService} from '../shared/data.service';
import {PodcastModel} from './podcast.model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.css'],
  providers: [ DataService]
})
export class PodcastsComponent implements OnInit {

  cardClassTypes: string [] = [
    'table-primary',
    // 'card text-white bg-secondary mb-3',
    'table-info',
    'table-success',
    'table-warning'
    // 'card text-white bg-primary mb-3',
    // 'card text-white bg-danger mb-3',
  ];

  choosenPodcast: string;

  filteredCategory = '';

  podcasts: PodcastModel[] = [

    new PodcastModel(
      'Lessons Learned While Helping Enterprises Adopt Machine Learning' ,
      'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/526594677',
      'Francesca Lazzeri',
      new Date('2018-11-17 14:00:00'), 'machine learning', '526594677')
  ];



  openModal(podcastId: string) {

    this.choosenPodcast = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/' + podcastId + '&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true';

  }


  constructor( private loggingService: LoggingService, private dataService: DataService, private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.loggingService.logToConsole('podcasts are fetched on initialize');

    this.dataService.getPodcasts().subscribe(
      podcasts => {

        this.podcasts = podcasts;
        this.loggingService.logToConsole('the size of podcasts: ' + podcasts.length);
      },
      (error: string) => {
        this.loggingService.logToConsole('error: ' + error); }

    );

  }

  setPodcasts(podcasts: PodcastModel[]) {
    this.podcasts = podcasts;
  }

  getPodcasts() {
    return this.podcasts.slice();
  }



}
