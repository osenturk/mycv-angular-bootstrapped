
export class AwardsModel {

  public authority: string;
  public name: string;
  public url: string;
  public category: string;
  public date: Date;
  public authorityUrl: string;

  constructor (authority: string, name: string, url: string, date: Date, category: string, authorityUrl: string) {
    this.authority = authority;
    this.name = name;
    this.url = url;
    this.date = date;
    this.category = category;
    this.authorityUrl = authorityUrl;
  }

}
