import { Component, OnInit } from '@angular/core';
import {DataService} from '../shared/data.service';
import {LoggingService} from '../logging.service';
import {PodcastModel} from '../podcasts/podcast.model';
import {AwardsModel} from './awards.model';


@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.css'],
  providers: [ DataService]
})
export class AwardsComponent implements OnInit {

  constructor(  private loggingService: LoggingService, private dataService: DataService) {}
  filteredCategory = '';

  awards: AwardsModel[] = [

    new AwardsModel(
      'Stanford University' ,
      'Machine Learning',
      'https://www.coursera.org/account/accomplishments/certificate/M4CLJRL3TQV6',
      new Date('2018-11-16'), 'Octave, Vectorization, Machine Learning' , 'https://www.coursera.org/' ) ];



  ngOnInit(): void {
    this.loggingService.logToConsole('awards are fetched on initialize');

    this.dataService.getAwards().subscribe(
      awards => {

        this.awards = awards;
        this.loggingService.logToConsole('the size of awards: ' + awards.length);
      },
      (error: string) => {
        this.loggingService.logToConsole('error: ' + error); }

    );

  }

}
