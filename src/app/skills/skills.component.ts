import { Component, OnInit } from '@angular/core';
import {LoggingService} from '../logging.service';
import {SkillModel} from './skill.model';

import {DataService} from '../shared/data.service';


@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css'],
  providers: [ DataService]
})
export class SkillsComponent implements OnInit {

  filteredCategory = '';

  badgeClassTypes: string [] = [
    'badge badge-pill badge-warning',
    'badge badge-pill badge-secondary',
    'badge badge-pill badge-info',
    'badge badge-pill badge-success',
    'badge badge-pill badge-primary',
    'badge badge-pill badge-danger',


    'badge badge-pill badge-light',
    'badge badge-pill badge-dark'
  ];


   skills: SkillModel[] = [

    new SkillModel(
      'Data Engineering' ,
      ['Principal Component Analysis (Dimension Reduction)', 'pandas', 'numpy']

    ),
    new SkillModel(
      'Statistics' ,
      ['Regression', 'learning curves', 'confusion matrix', 'z-scores', 'hypothesis test']

    )

  ];

  constructor( private loggingService: LoggingService, private dataService: DataService) {}

  ngOnInit(): void {
    this.loggingService.logToConsole('skills are fetched on initialize');

    this.dataService.getSkills().subscribe(
      skills => {

      this.skills = skills as SkillModel[];

      this.loggingService.logToConsole('the content of the skills: ' + skills[1]);
      this.loggingService.logToConsole('the size of skills: ' + skills.length);
    },
      (error: string) => {
        this.loggingService.logToConsole('error: ' + error); }

    );

  }

  setSkills(skills: SkillModel[]) {
    this.skills = skills;
  }

  getSkills() {
    return this.skills.slice();
  }


}
