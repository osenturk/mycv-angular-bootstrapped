export class SkillModel {

  public category: string;
  public skillSet: string[];

  constructor (category: string, skillSet: string[]) {
    this.category = category;
    this.skillSet = skillSet;
  }

}
