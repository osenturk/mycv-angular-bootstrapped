
export class AboutModel {

  public name: string;
  public surname: string;
  public address: string;
  public email: string;
  public summary: string;
  public linkedin: string;
  public tweeter: string;
  public github: string;

  constructor (name: string, surname: string, address: string, email: string, summary: string, linkedin: string,
                tweeter: string, github: string ) {
    this.name = name;
    this.surname = surname;
    this.address = address;
    this.email = email;
    this.summary = summary;
    this.linkedin = linkedin;
    this.tweeter = tweeter;
    this.github = github;
  }

}
