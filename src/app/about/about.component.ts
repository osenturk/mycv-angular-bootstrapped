import {Component, Input, OnInit} from '@angular/core';
import {LoggingService} from '../logging.service';
import {DataService} from '../shared/data.service';
import {AwardsModel} from '../awards/awards.model';
import {AboutModel} from './about.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  providers: [DataService]
})
export class AboutComponent implements OnInit {

  constructor(  private loggingService: LoggingService, private dataService: DataService) {}

  @Input() about: AboutModel =

    new AboutModel(
      'Ozan' ,
      'Senturk',
      'Biga', 'a@a.com' , 'there was a upon time',
       'linkedin', 'tweeter', 'github' ) ;


  ngOnInit() {

    this.loggingService.logToConsole('about is initialized');

    this.dataService.getAbout()

      .subscribe(
      about => {

        this.about = about;

      }, (error: string) => {
      this.loggingService.logToConsole('error: ' + error); }

    );

  }

}
