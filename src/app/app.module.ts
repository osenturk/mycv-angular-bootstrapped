import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { EducationComponent } from './education/education.component';
import { SkillsComponent } from './skills/skills.component';
import { PodcastsComponent } from './podcasts/podcasts.component';
import { AwardsComponent } from './awards/awards.component';
import { NavigationComponent } from './navigation/navigation.component';
import {FormsModule} from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import {LoggingService} from './logging.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { SanitizerPipe } from './sanitizer.pipe';
import { ChessComponent } from './chess/chess.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {LichessInterceptor} from './chess/model/lichess.interceptor';
import {ShortenPipe} from './shorten.pipe';



@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    EducationComponent,
    SkillsComponent,
    PodcastsComponent,
    AwardsComponent,
    NavigationComponent,
    FilterPipe,
    SanitizerPipe,
    ShortenPipe,
    ChessComponent


  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })

  ],
  providers: [LoggingService,
    {provide: HTTP_INTERCEPTORS, useClass: LichessInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
